import app from '@universis/api/dist/server/app';
import {ExpressDataApplication} from '@themost/express';
import {DataConfigurationStrategy} from '@themost/data';
import {CandidateService} from './CandidateService';



describe('UniversisCandidateService', () => {
    /**
     * @type {ExpressDataApplication}
     */
    let application;
    // noinspection JSValidateJSDoc
    /**
     * @type {ExpressDataContext}
     */
    let context;
    beforeAll(() => {
        /**
         * @type {ExpressDataApplication}
         */
        application = app.get(ExpressDataApplication.name);
    });

    beforeEach(async () => {
        context = application.createContext();
    });

    afterEach(done => {
        // important: clear cache after each test
        const configuration = context.getConfiguration();
        if (configuration.hasOwnProperty('cache')) {
            delete configuration.cache;
        }
        context.finalize(() => {
            return done();
        });
    });


    it('should create instance', () => {
        const service = new CandidateService(application);
        expect(service).toBeTruthy();
    });

    it('should install service', () => {
        /**
         * @type {DataConfigurationStrategy}
         */
        const dataConfiguration = application.getConfiguration().getStrategy(DataConfigurationStrategy);
        // validate service
        expect(application.getService(CandidateService)).toBeFalsy();
        // expect UserLoginAction to be undefined
        let model = dataConfiguration.getModelDefinition('StudyProgramRegisterAction');
        expect(model).toBeTruthy();
        // use service
        application.useService(CandidateService);
        //validate service instance
        expect(application.getService(CandidateService)).toBeTruthy();
        // validate model definition
        model = dataConfiguration.getModelDefinition('StudyProgramRegisterAction');
        expect(model).toBeTruthy();
    });

});
