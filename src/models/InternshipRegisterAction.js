import { DataObject, EdmMapping } from "@themost/data";

@EdmMapping.entityType('InternshipRegisterAction')
class InternshipRegisterAction extends DataObject {
    /**
     * Gets item review
     */
    @EdmMapping.func('review', 'InternshipRegisterActionReview')
    getReview() {
        return this.context.model('InternshipRegisterActionReview')
            .where('itemReviewed').equal(this.getId()).prepare();
    }

    /**
     * Set item review
     * @param {*} item
     */
    @EdmMapping.param('item', 'InternshipRegisterActionReview', true, true)
    @EdmMapping.action('review', 'InternshipRegisterActionReview')
    async setReview(item) {
        const InternshipRegisterActionReviews = this.context.model('InternshipRegisterActionReview');
        // infer object state
        const currentReview = await InternshipRegisterActionReviews.where('itemReviewed').equal(this.getId()).getItem();
        if (currentReview == null) {
            if (item == null) {
                return;
            }
            // a new item is going to be inserted
            delete item.id;
            // set reviewed item
            item.itemReviewed = this.getId();
        } else {
            if (item == null) {
                // delete review
                InternshipRegisterActionReviews.remove(currentReview);
            }
            // up
            item.id = currentReview.id;
            // set reviewed item
            item.itemReviewed = this.getId();
        }
        return InternshipRegisterActionReviews.save(item);
    }
}

module.exports = InternshipRegisterAction;