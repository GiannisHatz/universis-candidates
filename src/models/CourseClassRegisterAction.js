import { DataObject, EdmMapping } from "@themost/data";

@EdmMapping.entityType('CourseClasRegisterAction')
class CourseClasRegisterAction extends DataObject {
    /**
     * Gets item review
     */
    @EdmMapping.func('review', 'CourseClassRegisterActionReview')
    getReview() {
        return this.context.model('CourseClassRegisterActionReview')
            .where('itemReviewed').equal(this.getId()).prepare();
    }

    /**
     * Set item review
     * @param {*} item
     */
    @EdmMapping.param('item', 'CourseClassRegisterActionReview', true, true)
    @EdmMapping.action('review', 'CourseClassRegisterActionReview')
    async setReview(item) {
        const CourseClassRegisterActionReviews = this.context.model('CourseClassRegisterActionReview');
        // infer object state
        const currentReview = await CourseClassRegisterActionReviews.where('itemReviewed').equal(this.getId()).getItem();
        if (currentReview == null) {
            if (item == null) {
                return;
            }
            // a new item is going to be inserted
            delete item.id;
            // set reviewed item
            item.itemReviewed = this.getId();
        } else {
            if (item == null) {
                // delete review
                CourseClassRegisterActionReviews.remove(currentReview);
            }
            // up
            item.id = currentReview.id;
            // set reviewed item
            item.itemReviewed = this.getId();
        }
        return CourseClassRegisterActionReviews.save(item);
    }
}

module.exports = CourseClasRegisterAction;
