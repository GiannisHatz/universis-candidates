import StudyProgramRegisterAction from './StudyProgramRegisterAction';

class InternshipProgramRegisterAction extends StudyProgramRegisterAction {
    constructor() {
        super();
    }
}

module.exports = InternshipProgramRegisterAction;