import {ApplicationService} from '@themost/common';
import {DataContext} from '@themost/data';

export declare class RandomCandidateUserActivationCode extends ApplicationService {
    generate(context: DataContext, candidate: any): Promise<string>;
}