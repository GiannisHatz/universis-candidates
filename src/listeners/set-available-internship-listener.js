/**
 * @param {DataEventArgs} event
 */
async function afterSaveAsync(event) {
    const context = event.model.context;
    const item = await context.model('Internship')
        .where('id').equal(event.target.id)
        .silent().expand('status').getItem();
    // convert to available internship
    const AvailableInternships = context.model('AvailableInternship');
    const copyItem = {};
    // copy data
    AvailableInternships.attributes.forEach((attribute) => {
        copyItem[attribute.name] = item[attribute.name];
    });
    // validate existence
    const exists = await AvailableInternships.where('id').equal(event.target.id).silent().count();
    if (exists) {
        await AvailableInternships.silent().save(copyItem);
    } else {
        await AvailableInternships.silent().insert(copyItem);
    }
}

/**
 * @param {DataEventArgs} event
 */
async function afterRemoveAsync(event) {
    const context = event.model.context;
    const AvailableInternships = context.model('AvailableInternship');
    const exists = AvailableInternships.where('id').equal(event.target.id).silent().count();
    if (exists) {
        await AvailableInternships.silent().remove({
            id: event.target.id
        });
    }
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
    return afterSaveAsync(event).then(() => {
        return callback();
    }).catch((err) => {
        return callback(err);
    });
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterRemove(event, callback) {
    return afterRemoveAsync(event).then(() => {
        return callback();
    }).catch((err) => {
        return callback(err);
    });
}