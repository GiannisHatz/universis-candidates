import { getMailer } from '@themost/mailer';
import { DataError } from '@themost/data';
import {TraceUtils} from "@themost/common";
/**
 * @param {DataEventArgs} event
 */
async function afterSaveAsync(event) {
    if (event.state === 2) {
        const context = event.model.context;
        /**
         * @type {import('@themost/mailer').MailerHelper}
         */
        const mailer = getMailer(context);

        const actionStatus = await context.model('StudyProgramRegisterAction')
            .where('id').equal(event.target.id).select('actionStatus/alternateName').value();
        let previousStatus = 'UnknownActionStatus';
        if (event.previous) {
            previousStatus = event.previous.actionStatus.alternateName;
        } else {
            throw new DataError('E_STATE', 'The previous state of an object cannot be determined', null, 'StudyProgramRegisterAction');
        }
        if (actionStatus === previousStatus) {
            return;
        }
        // get mail template
        const mailTemplate = await context.model('MailConfiguration').where('target').equal('CandidateRegisterStatusMessage').silent().getItem();
        if (mailTemplate == null) {
            return;
        }
        // get parent action data
        const item = await context.model('StudyProgramRegisterAction')
            .where('id').equal(event.target.id)
            .expand({
                name: 'candidate',
                options: {
                    $expand: 'person'
                }
            }, 'owner')
            .silent().getItem();
        if (item) {
            const email = item.candidate.person.email;
            if (email== null || typeof email !== 'string') {
                TraceUtils.warn(`Cannot send email for ${item.candidate.person.familyName} ${item.candidate.person.givenName}. Candidate [${item.candidate.id}] email does not exist.`);
                return;
            }
            await new Promise((resolve) => {
                mailer.template(mailTemplate.template)
                    .subject(mailTemplate.subject)
                    .bcc(mailTemplate.bcc || '')
                    .to(email)
                    .send({
                        model: item
                    }, (err) => {
                        if (err) {
                            try {
                                TraceUtils.error('An error occurred while trying to send an email notification after status change.');
                                TraceUtils.error(`candidate=${item.candidate.id}, template=${mailTemplate.template}`);
                                TraceUtils.error(err);
                            } catch (err1) {
                                // do nothing
                            }
                            return resolve();
                        }
                        return resolve();
                    })
            });

        }
    }
}
/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
    return afterSaveAsync(event).then(() => {
        return callback();
    }).catch((err) => {
        return callback(err);
    });
}
