import { ApplicationService } from '@themost/common';
import { DataEventArgs } from '@themost/data';

export declare function afterSave(event: DataEventArgs, callback: (err?: Error) => void);

export declare class AfterCreateCandidateUser extends ApplicationService {
    install(): void;
}