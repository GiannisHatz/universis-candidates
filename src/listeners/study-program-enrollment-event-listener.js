/**
 * @param {DataEventArgs} event
 */
async function beforeRemoveAsync(event) {
    const context = event.model.context;
    // remove also attachment types
    const attachmentTypes = await context.model('EnrollmentAttachmentConfiguration').where('object').equal(event.target.identifier).silent().getItems();
    await context.model('EnrollmentAttachmentConfiguration').remove(attachmentTypes);
}
/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function beforeRemove(event, callback) {
    return beforeRemoveAsync(event).then(() => {
        return callback();
    }).catch( err => {
        return callback(err);
    });
}
