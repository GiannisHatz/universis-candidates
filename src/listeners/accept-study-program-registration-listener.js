import {DataObjectState, DataCacheStrategy, DataConfigurationStrategy} from "@themost/data";
import {DataError, AccessDeniedError, DataNotFoundError, TraceUtils, ApplicationService} from "@themost/common";
import path from "path";
/**
 * @param {DataEventArgs} event
 */
async function afterSaveAsync(event) {

    const context = event.model.context;
    // get current status
    const actionStatus = await context.model('StudyProgramRegisterAction')
        .where('id').equal(event.target.id).select('actionStatus/alternateName').value();

    if (actionStatus !== 'CompletedActionStatus') {
        return;
    }
    if (event.state === DataObjectState.Insert) {
        throw new AccessDeniedError('A study program registration cannot be completed automatically.', null);
    }
    if (event.state === DataObjectState.Update) {
        // get previous status
        let previousStatus = 'UnknownActionStatus';
        if (event.previous) {
            previousStatus = event.previous.actionStatus.alternateName;
        } else {
            throw new DataError('E_STATE', 'The previous state of an object cannot be determined', null, 'StudyProgramRegisterAction');
        }
        if (previousStatus === 'ActiveActionStatus') {
            // get register action
            const item = await context.model('StudyProgramRegisterAction')
                .where('id').equal(event.target.id)
                .expand('studyProgram', {
                    name: 'candidate',
                    options: {
                        $expand: 'person($expand=gender)'
                    }
                }, 'attachments')
                .silent().getItem();
            if (item == null) {
                throw new DataNotFoundError('Current item cannot be found or is inaccessible', null, 'StudyProgramRegisterAction');
            }
            /**
             * @type {Student}
             */
            let student = item.candidate.student ? context.model('Student').convert(item.candidate.student) : null;
            if (student == null) {
                // get study program data
                const studyProgramSpecialty = await context.model('StudyProgramSpecialty')
                    .where('studyProgram').equal(item.studyProgram).and('specialty').equal(-1).getItem();
                // create person
                const person = Object.assign({}, item.candidate.person);
                // set gender
                person.gender = item.candidate.person.gender.identifier;
                // remove person id in order to insert a person based on its data
                delete person.id;

                // create student
                // important note: each student has a unique for each study program
                const newStudent = Object.assign({}, item.candidate);
                delete newStudent.id;
                delete newStudent.studentIdentifier;
                delete newStudent.studentInstituteIdentifier;
                newStudent.person = person;
                newStudent.studyProgramSpecialty = studyProgramSpecialty;
                newStudent.semester = 1;
                newStudent.inscriptionDate = new Date();
                newStudent.inscriptionSemester = 1;
                newStudent.studentStatus = {
                    alternateName: 'active'
                };

                const Students = context.model('Student');
                student = await Students.save(newStudent);
                // update candidateStudent with new student id
                item.candidate.student = student.id;
                // save candidateStudent
                await context.model('CandidateStudent').save(item.candidate);
            }
            // add user to students group
            const userGroups = context.model('User').convert(item.owner).property('groups');
            // add to students group
            await userGroups.silent().insert({
                name: 'Students'
            });
            // and remove it from candidates
            await userGroups.silent().remove({
                name: 'Candidates'
            });
            // add also student attachments
            if (item.attachments) {
                const attachments = [];
                // add to student attachments
                for (let i = 0; i < item.attachments.length; i++) {
                    attachments.push(item.attachments[i].id);
                }
                student.attachments = attachments;
                await context.model('Student').silent().save(student);
            }
            const cache = context.getApplication().getConfiguration().getStrategy(DataCacheStrategy);
            if (cache != null) {
                cache.clear();
            }
        }
    }
}
/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
function afterSave(event, callback) {
    return afterSaveAsync(event).then(() => {
        return callback();
    }).catch((err) => {
        return callback(err);
    });
}


class CreateStudentAfterAcceptCandidate extends ApplicationService {
    constructor(app) {
        super(app);
        this.install('StudyProgramRegisterAction', __filename);
    }

    install() {
        /**
         * get data configuration
         * @type {DataConfigurationStrategy}
         */
        const configuration = this.getApplication().getConfiguration().getStrategy(DataConfigurationStrategy);
        // get StudentRequestAction model definition
        const model = configuration.getModelDefinition('StudyProgramRegisterAction');
        // get this file path relative to application execution path
        const listenerType = './' + path.relative(this.getApplication().getConfiguration().getExecutionPath(), __filename);
        // ensure model event listeners
        model.eventListeners = model.eventListeners || [];
        // try to find event listener
        const findIndex = model.eventListeners.findIndex(listener => {
            return listener.type === listenerType ||
                listener.type === listenerType.replace(/\.js$/, '');
        });
        if (findIndex < 0) {
            // add event listener
            model.eventListeners.push({
                type: listenerType
            });
            // update StudentRequestAction model definition
            configuration.setModelDefinition(model);
            // write to log
            TraceUtils.info(`Services: ${this.constructor.name} service has been successfully installed`);
        }
    }
}

export {
    afterSave,
    CreateStudentAfterAcceptCandidate
}
