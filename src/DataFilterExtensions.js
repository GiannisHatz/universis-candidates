import { DataFilterResolver } from '@themost/data/data-filter-resolver';

Object.assign(DataFilterResolver.prototype, {
    /**
     * @this DataFilterResolver
     * @param {*} callback
     */
    whoami: function(callback) {
        const context = this.context;
        if (context == null) {
            return callback(new Error('Current context cannot be empty.'));
        }
        if (context.user && context.user.name) {
            return callback(null, context.user && context.user.name);
        }
        return callback(new Error('Current user cannot be empty at this context.'));
    }
});
